import java.util.Scanner;
public class machine {
    public static void orderfood(String food){
        System.out.print("\nYou have ordered "+food+". ");
        System.out.println("Thank you!");
    }
    public static void main(String[] args) {

        int Tempura=100;
        int Ramen=200;
        int Udon=300;
        int Soba=400;
        int Sushi =500;
        int Sukiyaki=600;

        int Sum=0;
        int Continue=1;

        System.out.print("Welcome!\nWhat would you like to order?\n1. Tempura\n2. Ramen\n3. Udon\n4. Soba\n5.Sushi\n6. Sukiyaki\nYour order[1-6]:");

        while(Continue==1) {

        Scanner userIn1 = new Scanner(System.in);
        int choice1 = userIn1.nextInt();

            if (choice1 == 1) {
                orderfood("Tempura");
                Sum += Tempura;
            } else if (choice1 == 2) {
                orderfood("Ramen");
                Sum += Ramen;
            } else if (choice1 == 3) {
                orderfood("Udon");
                Sum += Udon;
            } else if (choice1 == 4) {
                orderfood("Soba");
                Sum += Soba;
            } else if (choice1 == 5) {
                orderfood("Sushi");
                Sum += Sushi;
            } else if (choice1 == 6) {
                orderfood("Sukiyaki");
                Sum += Sukiyaki;
            }

            System.out.print("Do you want to order others?\n[1.Yes or 2.No]:");

            Scanner userIn2 = new Scanner(System.in);
            int choice2 = userIn2.nextInt();

            if(choice2==2) {
                Continue=2;
                userIn1.close();
                userIn2.close();
            }else {
                System.out.print("What would you like to order:");
            }
        }

        System.out.println("\nTotal "+Sum+"yen\nThank you!");

    }
}