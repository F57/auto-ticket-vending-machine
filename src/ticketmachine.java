import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import static javax.swing.JOptionPane.showConfirmDialog;

public class ticketmachine {
    private JPanel root;
    private JLabel topLevel;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JButton sobaButton;
    private JButton sushiButton;
    private JButton sukiyakiButton;
    private JTextPane OrderedItemsList;
    private JButton CheckoutButton;
    private JLabel OrderedItems;
    private JLabel Totalfee;

    int sum=0;

    public void orderfood(String food,int price) {
        int confirming = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if (confirming == 0) {
            System.out.println("Order for " + food + " received.");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            sum += price;

            String currentText = OrderedItemsList.getText();
            OrderedItemsList.setText(currentText + food + " " + price +" yen");


            int confirming2 = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to have a drink as a set?\nIt's 200yen.",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if (confirming2 == 0) {
                    System.out.println("Order for a drink received.");
                    JOptionPane.showMessageDialog(null, "Order for a drink received.");
                    sum += 200;
                    String currentText2 = OrderedItemsList.getText();
                    OrderedItemsList.setText(currentText2 +"\ndrink set 200yen\n");

                }

            Totalfee.setText("Total cost " + sum + "yen\n");
            }
        }

    public ticketmachine(){

            tempuraButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderfood("Tempura", 500);
                }
            });
            tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("./jpgfile/Tempura.jpg")));


            ramenButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderfood("Ramen", 600);
                }
            });
            ramenButton.setIcon(new ImageIcon(this.getClass().getResource("./jpgfile/Ramen.jpg")));

            udonButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderfood("Udon", 450);
                }
            });
            udonButton.setIcon(new ImageIcon(this.getClass().getResource("./jpgfile/Udon.jpg")));

            sobaButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderfood("Soba", 550);
                }
            });
            sobaButton.setIcon(new ImageIcon(this.getClass().getResource("./jpgfile/Soba.jpg")));

            sushiButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderfood("Sushi", 900);
                }
            });
            sushiButton.setIcon(new ImageIcon(this.getClass().getResource("./jpgfile/Sushi.jpg")));

            sukiyakiButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    orderfood("Sukiyaki", 1000);
                }
            });
            sukiyakiButton.setIcon(new ImageIcon(this.getClass().getResource("./jpgfile/Sukiyaki.jpg")));

            OrderedItemsList.addComponentListener(new ComponentAdapter() {
                @Override
                public void componentResized(ComponentEvent e) {
                    super.componentResized(e);

                }
            });

            CheckoutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int confirming3 = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like to check out?",
                            "Check out Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );

                    System.out.println("The total price is " + sum + "yen.");
                    JOptionPane.showMessageDialog(null, "The total price is " + sum + "yen.");

                    if (confirming3 == 0) {
                        int confirming4 = JOptionPane.showConfirmDialog(
                                null,
                                "If you have made the payment, please press the OK button.",
                                "Payment Confirmation",
                                JOptionPane.YES_NO_OPTION
                        );


                        if(confirming4==0) {
                            System.out.print("Thank you very much! See you again.");
                            JOptionPane.showMessageDialog(null, "Thank you very much! See you again.");
                            sum = 0;
                            OrderedItemsList.setText("");
                            Totalfee.setText("Total cost");
                        }
                        else if(confirming4 == 1){
                            System.out.print("Please complete the payment.");
                            JOptionPane.showMessageDialog(null, "Please complete the payment.");
                        }
                    }
                }
            });

        }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ticket machine");
        frame.setContentPane(new ticketmachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}